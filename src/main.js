import Vue from 'vue'
import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.min.css';
import { auth } from './firebase'
import App from './App.vue'
import router from './router'
Vue.config.productionTip = false

Vue.use(Loading);

let app;

auth.onAuthStateChanged(user => {
  if (!app){
    app = new Vue({
      router,
      render: h => h(App)
    }).$mount('#app')
  }
});

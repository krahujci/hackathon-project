import Vue from 'vue'
import Router from 'vue-router'
import { db, auth } from './firebase'
import Home from './views/Home'
import Prihlaseni from './views/Prihlaseni'
import PridatNabidku from './views/PridatNabidku'
import PridatUzivatele from './views/PridatUzivatele'
import PridatNapad from './views/PridatNapad'
import Diskuze from './views/Diskuze'
import Vyhledat from './views/Vyhledat'
import Domacnost from './views/Domacnost'

Vue.use(Router)

let router = new Router({
  mode: "history",
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: {
        title: "Úvodní stránka"
      }
    },
    {
      path: '/prihlaseni',
      name: 'prihlaseni',
      component: Prihlaseni,
      meta: {
        title: "Přihlášení"
      },
      beforeEnter: (to, from, next) => {
        if (to.meta.user){
          next("/");
        }
        else {
          next();
        }
      }
    },
    {
      path: '/odhlaseni',
      name: 'odhlaseni',
      meta: {
        title: "Odhlášení"
      },
      beforeEnter: (to, from, next) => {
        auth.signOut().then(() => {
          next("/prihlaseni");
          location.reload();
        });
      }
    },
    {
      path: '/pridat-nabidku',
      name: 'pridat-nabidku',
      component: PridatNabidku,
      meta: {
        title: "Přidat nabídku",
        requiresAuth: true
      }
    },
    {
      path: '/pridat-napad',
      name: 'pridat-napad',
      component: PridatNapad,
      meta: {
        title: "Přidat nápad",
        requiresAuth: true
      }
    },
    {
      path: '/registrace',
      name: 'registrace',
      component: PridatUzivatele,
      meta: {
        title: "Registrace"
      },
      beforeEnter: (to, from, next) => {
        if (to.meta.user){
          next("/");
        }
        else {
          next();
        }
      }
    },
    {
      path: '/vyhledat',
      name: "vyhledat",
      component: Vyhledat,
      meta: {
        title: "Vyhledat"
      }
    },
    {
      path: '/diskuze',
      name: 'diskuze',
      component: Diskuze,
      meta: {
        title: "Diskuze"
      }
    },
    {
      path: '/domacnost/:domacnostId',
      name: 'domacnost',
      component: Domacnost,
      props: true,
      meta: {
        title: "Domácnost"
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  const currentUser = auth.currentUser;
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  
  if (currentUser){
    db.ref("/household").once("value", snapshot => {
      const households = snapshot.val();
      const object = Object.values(households).find(val => val.user == currentUser.uid);
      const objectIndex = Object.values(households).findIndex(val => val == object);

      const userHousehold = {
        id: Object.keys(households)[objectIndex],
        object
      }

      if (userHousehold.id){
        to.meta.user = {
          uuid: currentUser.uid,
          householdId: userHousehold.id,
          name: userHousehold.object.name
        };
      }

      next();
    });
  }
  else {
    if (requiresAuth){
      next("/prihlaseni");
    }
    else {
      next();
    }
  }
});

export default router;

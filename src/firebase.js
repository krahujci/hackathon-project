import firebase from 'firebase'

const config = {
    apiKey: "AIzaSyBgTrMxTDLeuPM4h41KhklGea5ZQRuJoJM",
    authDomain: "hackathon-64f2f.firebaseapp.com",
    databaseURL: "https://hackathon-64f2f.firebaseio.com",
    projectId: "hackathon-64f2f",
    storageBucket: "hackathon-64f2f.appspot.com",
    messagingSenderId: "256404959082"
};

const firebaseApp = firebase.initializeApp(config)
const db = firebaseApp.database();
const storage = firebaseApp.storage();
const auth = firebaseApp.auth();

export {
    db,
    storage,
    auth
}